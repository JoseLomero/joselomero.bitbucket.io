document.getElementById("traducir").addEventListener("click", separadorYPrintador);

/**
 * Separa el texto del input y lo introduce en una array que lo traducira
 */
function separadorYPrintador(){
    var textoEntrada = document.getElementById("usuario-texto").value;
    var separado = textoEntrada.split("");
    
    traductor(separado);
}

/**
 * Recoge el array y reemplaza las vocales por i
 * @param {array} array contiene el texto a traducir separado por vocales
 */
function traductor(array) {
    array.forEach(function(item, i) { if (item == "a" || item == "e" || item == "o" || item == "u") array[i] = "i"; });
    var text = array.join('');
    document.getElementById("texto-traducido").innerHTML = text;
}